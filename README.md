Script to export all photos store in Photos app

# How to run

```shell script
python osxphotos-exporter.py ${destination}
```

# Library
- https://pypi.org/project/progress/
- https://github.com/RhetTbull/osxphotos